using UnityEngine;

namespace Scoring.Data
{
    [CreateAssetMenu(fileName = "NewBestScore", menuName = "SO/Scoring/BestScore")]
    public class BestScoreSO : ScriptableObject
    {
        [field: SerializeField] public int Value { get; private set; }

        public void Save(int newValue)
        {
            if (newValue > Value)
            {
                Value = newValue;
            }
        }
    }
}