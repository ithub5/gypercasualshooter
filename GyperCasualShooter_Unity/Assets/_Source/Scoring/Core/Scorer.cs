using System;

namespace Scoring.Core
{
    public class Scorer : Abstract.Observer.IObserver<int>
    {
        public int Score { get; private set; }

        private Abstract.Observer.IObservable<int>[] _observables;

        public event Action OnScoreChanged;
        public event Action OnScoreNegative;

        public Scorer(Abstract.Observer.IObservable<int>[] observables)
        {
            _observables = observables;
        }

        public void Bind()
        {
            foreach (var observable in _observables)
            {
                observable.AddObserver(this);
            }
        }
        
        public void Update(int value)
        {
            Score += value;
            OnScoreChanged?.Invoke();
            
            if (Score < 0)
            {
                OnScoreNegative?.Invoke();
            }
        }
    }
}