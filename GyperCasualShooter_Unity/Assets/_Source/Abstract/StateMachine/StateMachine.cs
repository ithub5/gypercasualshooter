using System;
using System.Linq;

namespace Abstract.StateMachine
{
    public class StateMachine
    {
        protected AState[] states;
        
        protected AState currentState;
        
        public StateMachine(params AState[] states)
        {
            this.states = states;
        }

        public void ToState(Type nextStateType)
        {
            AState nextState = states.FirstOrDefault(state => state.GetType() == nextStateType);
            if (nextState == default)
            {
                return;
            }
            
            currentState?.Exit();
            currentState = nextState;
            currentState.Enter();
        }
    }
}