namespace Abstract.Observer
{
    public interface IObserver<T>
    {
        public void Update(T value);
    }
}