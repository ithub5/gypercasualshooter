namespace Abstract.Observer
{
    public interface IObservable<T>
    {
        public void AddObserver(IObserver<T> o);
        public void RemoveObserver(IObserver<T> o);
        public void Notify();
    }
}