using LevelSystem.Data;
using UnityEditor;
using UnityEngine;

namespace CustomEditors.LevelSystem
{
    [CustomEditor(typeof(LevelSO))]
    public class LevelSOEditor : Editor
    {
        private LevelSO _castedTarget;

        private SerializedProperty _platesKeys;
        private SerializedProperty _platesValues;
        private SerializedProperty _obstacles;
        private SerializedProperty _obstaclesRotationSpeed;

        private DamageableSO _tempKey;
        private int _tempValue;

        private GUILayoutOption _dictionaryValueOption;

        private void OnEnable()
        {
            _castedTarget = target as LevelSO;

            SerializedProperty iterator = serializedObject.GetIterator();
            iterator.NextVisible(true);
            iterator.NextVisible(false);
            _platesKeys = iterator.Copy();
            iterator.NextVisible(false);
            _platesValues = iterator.Copy();
            iterator.NextVisible(false);
            _obstaclesRotationSpeed = iterator.Copy();
            iterator.NextVisible(false);
            _obstacles = iterator.Copy();
        }

        public override void OnInspectorGUI()
        {
            if (_castedTarget.Plates.Count > 0)
            {
                EditorGUILayout.LabelField("Plates", EditorStyles.boldLabel);
                DrawDictionary();
                            
                GUILayout.Space(5f);
            }
            
            EditorGUILayout.LabelField("Add Plate", EditorStyles.boldLabel);
            DrawAddToDictionary();
            
            GUILayout.Space(10f);
            EditorGUILayout.PropertyField(_obstaclesRotationSpeed);
            EditorGUILayout.PropertyField(_obstacles);
            serializedObject.ApplyModifiedProperties();
        }

        private void DrawDictionary()
        {
            for (int i = 0; i < _platesKeys.arraySize; i++)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.PropertyField(_platesKeys.GetArrayElementAtIndex(i), GUIContent.none);
                EditorGUILayout.PropertyField(_platesValues.GetArrayElementAtIndex(i), GUIContent.none);
                if (GUILayout.Button("-"))
                {
                    _platesKeys.DeleteArrayElementAtIndex(i);
                    _platesValues.DeleteArrayElementAtIndex(i);
                }
                EditorGUILayout.EndHorizontal();
            }

            serializedObject.ApplyModifiedProperties();
        }

        private void DrawAddToDictionary()
        {
            EditorGUILayout.BeginHorizontal();
            _tempKey = EditorGUILayout.ObjectField(_tempKey, typeof(DamageableSO)) as DamageableSO;
            _tempValue = EditorGUILayout.IntField(_tempValue);
            if (GUILayout.Button("+") && _tempKey != null && !_castedTarget.Plates.ContainsKey(_tempKey))
            {
                _platesKeys.arraySize++;
                _platesKeys.GetArrayElementAtIndex(_platesKeys.arraySize - 1).objectReferenceValue = _tempKey;
                _platesValues.arraySize++;
                _platesValues.GetArrayElementAtIndex(_platesValues.arraySize - 1).intValue = _tempValue;
            }
            EditorGUILayout.EndHorizontal();
        }
    }
}