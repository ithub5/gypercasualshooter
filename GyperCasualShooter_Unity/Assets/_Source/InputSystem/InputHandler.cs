
namespace InputSystem
{
    public class InputHandler
    {
        private MainActions _actions;

        public MainActions.CharacterActions CharacterActions => _actions.Character;
        
        public InputHandler()
        {
            _actions = new MainActions();
        }

        public void Enable()
        {
            _actions.Enable();
        }

        public void Disable()
        {
            _actions.Disable();
        }
    }
}