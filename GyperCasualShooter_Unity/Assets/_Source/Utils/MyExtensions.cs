using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Utils
{
    public static class MyExtensions
    {
        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> collection)
        {
            return collection.OrderBy(a => Random.value);
        }
    }
}