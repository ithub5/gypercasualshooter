using UnityEngine.SceneManagement;

namespace Utils.Services
{
    public static class SceneJumper
    {
        public static void LoadMenu() =>
            SceneManager.LoadScene(0);
        
        public static void LoadGameplay() =>
            SceneManager.LoadScene(1);
    }
}