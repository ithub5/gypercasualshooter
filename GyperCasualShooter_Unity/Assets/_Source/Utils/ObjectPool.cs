using System.Collections.Generic;

namespace Utils
{
    public class ObjectPool<T>
    {
        private Queue<T> _pool;

        public ObjectPool(T[] objects)
        {
            _pool = new Queue<T>(objects);
        }

        public T Get() =>
            _pool.Dequeue();

        public void Return(T obj) =>
            _pool.Enqueue(obj);
    }
}