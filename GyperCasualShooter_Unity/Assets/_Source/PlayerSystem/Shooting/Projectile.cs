using System;
using System.Collections;
using LevelSystem.Abstraction;
using UnityEngine;

namespace PlayerSystem.Shooting
{
    public class Projectile : MonoBehaviour
    {
        [SerializeField] private Rigidbody rb;
        [SerializeField] private float travelSpeed;
        [SerializeField] private float lifeTime;
        public float EnergyCost { get; private set; }

        public event Action<Projectile> OnLifeTimeEnd;

        public void Init(float energyCost)
        {
            EnergyCost = energyCost;
        }
        
        private void OnTriggerEnter(Collider col)
        {
            if (!col.TryGetComponent(out IDamageable target))
            {
                return;
            }
            
            target.TakeDamage();
            OnLifeTimeEnd?.Invoke(this);
        }

        public void ShootSelf()
        {
            StartCoroutine(LifeTimeCounter());
            rb.velocity = transform.forward * travelSpeed;
        }

        private IEnumerator LifeTimeCounter()
        {
            yield return new WaitForSeconds(lifeTime);
            OnLifeTimeEnd?.Invoke(this);
        }
    }
}