using System;
using UnityEngine;
using System.Collections.Generic;
using PlayerSystem.Data.Core;
using Utils;

namespace PlayerSystem.Shooting
{
    public class AmmoPoolInitializer
    {
        private PlayerAmmoSO _ammoSO;
        private PlayerEnergySO _energySO;
        
        private Transform _firePointTransform;

        public AmmoPoolInitializer(PlayerAmmoSO ammoSo, PlayerEnergySO energySo, Transform firePointTransform)
        {
            _ammoSO = ammoSo;
            _energySO = energySo;
            _firePointTransform = firePointTransform;
        }

        public ObjectPool<Projectile> InitPlayerAmmo()
        {
            List<Projectile> ammo = new List<Projectile>();

            int neededAmmoAmount = CountNeededAmmoAmount();
            for (int i = 0; i < neededAmmoAmount; i++)
            {
                Projectile newProjectile = _ammoSO.MainProjectileSO.GetInstance(_firePointTransform);
                newProjectile.gameObject.SetActive(false);
                
                ammo.Add(newProjectile);
            }
            return new ObjectPool<Projectile>(ammo.ToArray());
        }
        
        private int CountNeededAmmoAmount()
        {
            float oneProjectileCost = _ammoSO.MainProjectileSO.EnergyCost;
            float chargePerSecond = _energySO.MaxAmount / _energySO.FullRechargeTime;

            int oneFullProjectilesCharge = (int)MathF.Ceiling(_energySO.MaxAmount / oneProjectileCost);
            int neededAmmoCount = oneFullProjectilesCharge + (int)MathF.Ceiling(oneFullProjectilesCharge * chargePerSecond);
            return neededAmmoCount;
        }   
    }
}