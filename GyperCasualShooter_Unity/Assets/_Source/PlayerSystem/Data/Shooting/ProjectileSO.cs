using PlayerSystem.Shooting;
using UnityEngine;

namespace PlayerSystem.Data.Shooting
{
    [CreateAssetMenu(fileName = "NewProjectile", menuName = "SO/Player/Shooting/Projectile")]
    public class ProjectileSO : ScriptableObject
    {
        [field: SerializeField] public GameObject Prefab { get; private set; }
        [field: SerializeField] public float EnergyCost { get; private set; }

        public Projectile GetInstance(Transform parentObject)
        {
            Projectile projectile = Instantiate(Prefab, parentObject).GetComponent<Projectile>();
            projectile.Init(EnergyCost);

            return projectile;
        }
    }
}