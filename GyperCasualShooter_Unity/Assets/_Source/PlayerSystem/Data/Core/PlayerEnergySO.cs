using UnityEngine;

namespace PlayerSystem.Data.Core
{
    [CreateAssetMenu(fileName = "PlayerEnergy", menuName = "SO/Player/Energy")]
    public class PlayerEnergySO : ScriptableObject
    {
        [field: SerializeField] public float MaxAmount { get; private set; }
        [field: SerializeField] public float FullRechargeTime { get; private set; }
        
        [field: SerializeField] public float UpdateDeltaTime { get; private set; }
    }
}