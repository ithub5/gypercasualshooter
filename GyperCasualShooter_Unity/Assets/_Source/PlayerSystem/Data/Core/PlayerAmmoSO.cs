using PlayerSystem.Data.Shooting;
using UnityEngine;

namespace PlayerSystem.Data.Core
{
    [CreateAssetMenu(fileName = "PlayerAmmo", menuName = "SO/Player/Ammo")]
    public class PlayerAmmoSO : ScriptableObject
    {
        [field: SerializeField] public ProjectileSO MainProjectileSO { get; private set; }
    }
}