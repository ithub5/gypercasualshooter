using InputSystem;
using UnityEngine.InputSystem;

namespace PlayerSystem.Core
{
    public class PlayerInvoker
    {
        private InputHandler _input;
        private PlayerEnergy _energy;
        private PlayerShooter _shooter;

        public PlayerInvoker(InputHandler input, PlayerEnergy energy, PlayerShooter shooter)
        {
            _input = input;
            _energy = energy;
            _shooter = shooter;
        }

        public void Bind()
        {
            _input.CharacterActions.Fire.performed += PerformShoot;
        }
        
        public void Expose()
        {
            _input.CharacterActions.Fire.performed -= PerformShoot;
        }

        private void PerformShoot(InputAction.CallbackContext ctx)
        {
            if (!CheckEnergyForShoot())
            {
                return;
            }

            _energy.AddEnergy(-_shooter.NextProjectileEnergy);
            _shooter.Shoot();
        }

        private bool CheckEnergyForShoot() =>
            _energy.CurrentAmount >= _shooter.NextProjectileEnergy;
    }
}