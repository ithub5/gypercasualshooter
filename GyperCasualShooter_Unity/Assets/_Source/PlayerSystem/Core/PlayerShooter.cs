using PlayerSystem.Shooting;
using UnityEngine;
using Utils;

namespace PlayerSystem.Core
{
    public class PlayerShooter
    {
        private Transform _firePoint;
        
        private ObjectPool<Projectile> _projectilePool;

        private Projectile _nextProjectile;

        public float NextProjectileEnergy => _nextProjectile.EnergyCost;

        public PlayerShooter(Transform firePoint, ObjectPool<Projectile> projectilePool)
        {
            _firePoint = firePoint;
            _projectilePool = projectilePool;
            
            _nextProjectile = _projectilePool.Get();
        }

        public void Shoot()
        {
            _nextProjectile.gameObject.SetActive(true);
            
            _nextProjectile.transform.position = _firePoint.position;
            _nextProjectile.transform.rotation = _firePoint.rotation;

            _nextProjectile.OnLifeTimeEnd += ReturnProjectileToPool;
            
            _nextProjectile.ShootSelf();
            
            _nextProjectile = _projectilePool.Get();
        }

        private void ReturnProjectileToPool(Projectile target)
        {
            target.OnLifeTimeEnd -= ReturnProjectileToPool;
            
            target.gameObject.SetActive(false);
            _projectilePool.Return(target);
        }
    }
}