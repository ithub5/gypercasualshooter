using System;
using DG.Tweening;
using PlayerSystem.Data.Core;

namespace PlayerSystem.Core
{
    public class PlayerEnergy
    {
        private PlayerEnergySO _settingsSO;
        
        private float _chargePerSecond;

        private Sequence _rechargeSequence;

        public float CurrentAmount { get; private set; }
        
        public event Action OnEnergyChanged;
        
        public PlayerEnergy(PlayerEnergySO settingsSO)
        {
            _settingsSO = settingsSO; 
            
            _chargePerSecond = _settingsSO.MaxAmount / _settingsSO.FullRechargeTime ;
            InitSequence();
        }
        
        /// <summary>
        /// adds energy (can be negative)
        /// </summary>
        public void AddEnergy(float amount)
        {
            CurrentAmount += amount;
            OnEnergyChanged?.Invoke();

            if (CurrentAmount < _settingsSO.MaxAmount && !_rechargeSequence.IsPlaying())
            {
                _rechargeSequence.Restart();
            }
        }
        
        private void ChargeEnergy()
        {
            float energyToCharge = _chargePerSecond * _settingsSO.UpdateDeltaTime;
            if (CurrentAmount + energyToCharge > _settingsSO.MaxAmount)
            {
                AddEnergy(_settingsSO.MaxAmount - CurrentAmount);
                _rechargeSequence.Rewind();
            }
            else
            {
                AddEnergy(energyToCharge);
                _rechargeSequence.Restart();
            }
        }

        private void InitSequence()
        {
            _rechargeSequence = DOTween.Sequence();
            _rechargeSequence.SetAutoKill(false);

            _rechargeSequence.AppendInterval(_settingsSO.UpdateDeltaTime);
            _rechargeSequence.AppendCallback(ChargeEnergy);
        }
    }
}