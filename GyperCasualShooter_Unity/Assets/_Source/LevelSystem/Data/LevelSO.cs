using System.Collections.Generic;
using UnityEngine;
using Utils.Attributes;

namespace LevelSystem.Data
{
    [CreateAssetMenu(fileName = "NewLevel", menuName = "SO/Level/Level")]
    public class LevelSO : ScriptableObject
    {
        [SerializeField, ReadOnly] private List<DamageableSO> keys = new List<DamageableSO>();
        [SerializeField] private List<int> values = new List<int>();

        [field: SerializeField] public float ObstaclesRotationSpeed { get; private set; }
        [field: SerializeField] public DamageableSO[] Obstacles { get; private set; }

        private Dictionary<DamageableSO, int> _plates = new Dictionary<DamageableSO, int>();
        
        /// <summary>
        /// generates every time (better to cash in a field)
        /// </summary>
        public Dictionary<DamageableSO, int> Plates
        {
            get
            {
                _plates = new Dictionary<DamageableSO, int>();
                for (int i = 0; i < keys.Count; i++)
                {
                    _plates.Add(keys[i], values[i]);
                }

                return _plates;
            }
        }
    }
}