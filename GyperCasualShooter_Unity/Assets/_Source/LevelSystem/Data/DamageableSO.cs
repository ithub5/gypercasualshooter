using LevelSystem.Core;
using UnityEngine;

namespace LevelSystem.Data
{
    [CreateAssetMenu(fileName = "NewDamageable", menuName = "SO/Level/Damageable")]
    public class DamageableSO : ScriptableObject
    {
        [field: SerializeField] public GameObject Prefab { get; private set; }
        [SerializeField] private int score;
        
        public virtual Damageable GetInstance(Transform parentObject)
        { 
            Damageable damageable = Instantiate(Prefab, parentObject).GetComponent<Damageable>();
            damageable.Init(score);

            return damageable;
        }
    }
}