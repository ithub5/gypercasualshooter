using System;

namespace LevelSystem.Abstraction
{
    public interface IDamageable
    {
        public void TakeDamage();

        public event Action OnDestroy;
    }
}