using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using LevelSystem.Data;
using UnityEngine;
using Utils;

namespace LevelSystem.Core
{
    public class LevelBuilder
    {
        private LevelSO _levelSO;
        private Transform _platesParent;
        private Transform _obstaclesParent;
        
        private Dictionary<DamageableSO, int> _shuffledPlates;
        private List<Damageable> _plates;
        private List<Damageable> _obstacles;

        private Sequence rotationSequence;

        /// <summary>
        /// generates everytime
        /// </summary>
        public Damageable[] Plates => _plates.ToArray();
        public Damageable[] Obstacles => _obstacles.ToArray();

        public LevelBuilder(LevelSO levelSO, Transform platesParent, Transform obstaclesParent)
        {
            _levelSO = levelSO;
            _platesParent = platesParent;
            _obstaclesParent = obstaclesParent;

            InitPlates();
            InitObstacles();
            InitRotationSequence();
        }

        public void Build()
        {
            for (int i = 0; i < _plates.Count; i++)
            {
                _plates[i].transform.position += Vector3.up * (i + 1);
                _plates[i].gameObject.SetActive(true);
            }

            foreach (var obstacle in _obstacles)
            {
                obstacle.gameObject.SetActive(true);
            }
            
            StartRotation();
        }

        private void InitPlates()
        {
            _shuffledPlates = _levelSO.Plates.Shuffle().ToDictionary(
                plate => plate.Key, count => count.Value);
            _plates = new List<Damageable>();

            foreach (var plate in _shuffledPlates.Keys)
            {
                for (int i = 0; i < _shuffledPlates[plate]; i++)
                {
                    Damageable newPlate = plate.GetInstance(_platesParent);
                    newPlate.gameObject.SetActive(false);
                    _plates.Add(newPlate);
                }
            }

            _plates = _plates.Shuffle().ToList();
        }

        private void InitObstacles()
        {
            DamageableSO[] obstacles = _levelSO.Obstacles;
            _obstacles = new List<Damageable>();
            
            foreach (var obstacle in obstacles)
            {
                _obstacles.Add(obstacle.GetInstance(_obstaclesParent));
            }
        }

        private void InitRotationSequence()
        {
            rotationSequence = DOTween.Sequence();
            rotationSequence.SetAutoKill(false);

            rotationSequence.AppendCallback(() => _obstaclesParent.RotateAround(_obstaclesParent.position, Vector3.up, _levelSO.ObstaclesRotationSpeed));
            rotationSequence.AppendInterval(0.01f);
            rotationSequence.AppendCallback(() => rotationSequence.Restart());

            rotationSequence.Pause();
        }

        private void StartRotation()
        {
            rotationSequence.Play();
        }

        public void StopRotation()
        {
            rotationSequence.Kill();
        }
    }
}