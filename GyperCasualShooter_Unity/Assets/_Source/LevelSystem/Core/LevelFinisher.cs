using System;
using LevelSystem.Abstraction;
using Scoring.Core;

namespace LevelSystem.Core
{
    public class LevelFinisher
    {
        private IDamageable[] _plates;
        private Scorer _scorer;
        
        private int _platesCounter;

        public event Action OnFinish;

        public LevelFinisher(IDamageable[] plates, Scorer scorer)
        {
            _plates = plates;
            _scorer = scorer;

            _platesCounter = _plates.Length;
        }

        public void Bind()
        {
            foreach (var plate in _plates)
            {
                plate.OnDestroy += CountDown;
            }

            _scorer.OnScoreNegative += Lose;
        }

        private void CountDown()
        {
            _platesCounter--;
            if (_platesCounter == 0)
            {
                OnFinish?.Invoke();
            }
        }
        
        private void Lose()
        {
            OnFinish?.Invoke();
        }
    }
}