using System;
using System.Collections.Generic;
using LevelSystem.Abstraction;
using UnityEngine;

namespace LevelSystem.Core
{
    public class Damageable : MonoBehaviour, IDamageable, Abstract.Observer.IObservable<int>
    {
        public int Score { get; private set; }

        private List<Abstract.Observer.IObserver<int>> _observers = new List<Abstract.Observer.IObserver<int>>();

        public event Action OnDestroy;

        public void Init(int score)
        {
            Score = score;
        }
        
        public void TakeDamage()
        {
            Notify();
            for (int i = _observers.Count - 1; i < 0; i--)
            {
                RemoveObserver(_observers[i]);
            }
            gameObject.SetActive(false);
            OnDestroy?.Invoke();
        }

        public void AddObserver(Abstract.Observer.IObserver<int> o)
        {
            if (_observers.Contains(o))
            {
                return;
            }
            _observers.Add(o);
        }

        public void RemoveObserver(Abstract.Observer.IObserver<int> o)
        {
            _observers.Remove(o);
        }

        public void Notify()
        {
            foreach (var observer in _observers)
            {
                observer.Update(Score);
            }
        }
    }
}