using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "Paths", menuName = "SO/Paths")]
    public class PathsSO : ScriptableObject
    {
        [field: SerializeField] public string LevelsPath { get; private set; }
    }
}