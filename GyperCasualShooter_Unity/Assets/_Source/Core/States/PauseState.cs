using Abstract.StateMachine;

namespace Core.States
{
    public class PauseState : AState
    {
        private Game _game;
        
        public PauseState(Game game)
        {
            _game = game;
        }
        
        public override void Enter()
        {
            _game.Stop();
        }

        public override void Exit()
        {
            
        }
    }
}