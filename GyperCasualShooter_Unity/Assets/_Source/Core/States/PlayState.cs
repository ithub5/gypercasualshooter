using Abstract.StateMachine;

namespace Core.States
{
    public class PlayState : AState
    {
        private Game _game;
        
        public PlayState(Game game)
        {
            _game = game;
        }
        
        public override void Enter()
        {
            _game.Start();
        }

        public override void Exit()
        {
            
        }
    }
}