using System.Linq;
using Abstract.StateMachine;
using Core.States;
using Data;
using InputSystem;
using LevelSystem.Core;
using LevelSystem.Data;
using PlayerSystem.Core;
using PlayerSystem.Data.Core;
using PlayerSystem.Shooting;
using Scoring.Core;
using Scoring.Data;
using UI.Energy;
using UI.Score;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Core
{
    public class Bootstrapper : MonoBehaviour
    {
        [Header("Player")]
        [SerializeField] private Transform firePointTransform;

        [SerializeField] private PlayerEnergySO playerEnergySO;
        [SerializeField] private PlayerAmmoSO playerAmmoSO;

        [Header("UI")] 
        [SerializeField] private ScoreView scoreUIView;
        [SerializeField] private EnergyView energyUIView;
        [SerializeField] private GameObject menu;

        [Header("Level")] 
        [SerializeField] private Transform platesParentTransform;
        [SerializeField] private Transform obstaclesParentTransform;

        [Header("Data")]
        [SerializeField] private PathsSO pathsSO;
        [SerializeField] private BestScoreSO bestScoreSO;

        private Game _game;
        private InputHandler _input;

        private PlayerInvoker _playerInvoker;
        private PlayerEnergy _playerEnergy;
        private PlayerShooter _playerShooter;
        
        private ScoreController _scoreUIController;
        private EnergyController _energyUIController;

        private LevelBuilder _levelBuilder;
        
        private Scorer _scorer;

        private LevelFinisher _levelFinisher;

        private StateMachine _gameStateMachine;
    
        private void Awake()
        {
            _input = new InputHandler();
            
            InitPlayer();
            InitLevel();
            InitScoring();
            InitUI();
            _levelFinisher = new LevelFinisher(_levelBuilder.Plates, _scorer);

            _game = new Game(_input, _playerInvoker, _levelBuilder, _scorer, _levelFinisher, 
                menu, _scoreUIController, _energyUIController, bestScoreSO);
            InitStateMachine();
        }

        private void InitPlayer()
        {
            _playerEnergy = new PlayerEnergy(playerEnergySO);
            AmmoPoolInitializer ammoPoolInitializer = new AmmoPoolInitializer(playerAmmoSO, playerEnergySO, firePointTransform);
            _playerShooter = new PlayerShooter(firePointTransform, ammoPoolInitializer.InitPlayerAmmo());
            _playerInvoker = new PlayerInvoker(_input, _playerEnergy, _playerShooter);
        }

        private void InitLevel()
        {
            LevelSO[] levels = Resources.LoadAll<LevelSO>(pathsSO.LevelsPath);
            LevelSO randomLevel = levels[Random.Range(0, levels.Length)];
            _levelBuilder = new LevelBuilder(randomLevel, platesParentTransform, obstaclesParentTransform);
        }
        
        private void InitScoring()
        {
            _scorer = new Scorer(_levelBuilder.Plates.Concat(_levelBuilder.Obstacles).ToArray());
        }
        
        private void InitUI()
        {
            _scoreUIController = new ScoreController(_scorer, scoreUIView);
            _energyUIController = new EnergyController(_playerEnergy, energyUIView, playerEnergySO);
        }

        private void InitStateMachine()
        {
            AState playState = new PlayState(_game);
            AState pauseState = new PauseState(_game);

            _gameStateMachine = new StateMachine(playState, pauseState);
            _gameStateMachine.ToState(typeof(PlayState));

            _levelFinisher.OnFinish += () => _gameStateMachine.ToState(typeof(PauseState));
        }
    }
}