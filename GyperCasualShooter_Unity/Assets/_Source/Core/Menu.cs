using System;
using Scoring.Data;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils.Services;

namespace Core
{
    public class Menu : MonoBehaviour
    {
        [SerializeField] private BestScoreSO bestScoreSO;
        [SerializeField] private TextMeshProUGUI bestScoreText;
        [SerializeField] private Button playButton;
        [SerializeField] private Button menuButton;

        private void OnEnable()
        {
            bestScoreText.text = bestScoreSO.Value.ToString();
            
            playButton.onClick.AddListener(SceneJumper.LoadGameplay);
            if (menuButton == null)
            {
                return;
            }
            menuButton.onClick.AddListener(SceneJumper.LoadMenu);
        }

        private void OnDisable()
        {
            playButton.onClick.RemoveAllListeners();
            if (menuButton == null)
            {
                return;
            }
            menuButton.onClick.RemoveAllListeners();
        }
    }
}