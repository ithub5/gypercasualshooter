using InputSystem;
using LevelSystem.Core;
using PlayerSystem.Core;
using Scoring.Core;
using Scoring.Data;
using UI.Energy;
using UI.Score;
using UnityEngine;

namespace Core
{
    public class Game
    {
        private InputHandler _input;

        private PlayerInvoker _playerInvoker;
        private LevelBuilder _levelBuilder;
        private Scorer _scorer;
        private LevelFinisher _levelFinisher;

        private GameObject _menu;
        private ScoreController _scoreUIController;
        private EnergyController _energyUIController;
        private BestScoreSO _bestScoreSO;

        public Game(InputHandler input, PlayerInvoker playerInvoker, LevelBuilder levelBuilder, Scorer scorer, 
            LevelFinisher levelFinisher, GameObject menu, ScoreController scoreUIController, EnergyController energyUIController,
            BestScoreSO bestScoreSO) 
        {
            _input = input;
            _playerInvoker = playerInvoker;
            _levelBuilder = levelBuilder;
            _scorer = scorer;
            _levelFinisher = levelFinisher;

            _menu = menu;
            _scoreUIController = scoreUIController;
            _energyUIController = energyUIController;
            _bestScoreSO = bestScoreSO;
            
            _levelBuilder.Build();
        }

        public void Start()
        {
            _input.Enable();
            _menu.SetActive(false);
            Bind();
        }

        public void Stop()
        {
            _input.Disable();
            _bestScoreSO.Save(_scorer.Score);
            _levelBuilder.StopRotation();
            _menu.SetActive(true);
            Expose();
        }

        private void Bind()
        {
            _playerInvoker.Bind();
            _energyUIController.Bind();
            _scorer.Bind();
            _levelFinisher.Bind();
            _scoreUIController.Bind();
            _energyUIController.Bind();
        }

        private void Expose()
        {
            _playerInvoker.Expose();
            _energyUIController.Expose();
            _scoreUIController.Expose();
        }
    }
}