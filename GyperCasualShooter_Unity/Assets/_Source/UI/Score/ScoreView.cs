using TMPro;
using UnityEngine;

namespace UI.Score
{
    public class ScoreView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _text;

        public void UpdateValue(int newScore)
        {
            _text.text = newScore.ToString();
        }
    }
}