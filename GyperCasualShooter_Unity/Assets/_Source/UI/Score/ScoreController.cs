using Scoring.Core;

namespace UI.Score
{
    public class ScoreController
    {
        private Scorer _scorer;
        
        private ScoreView _view;

        public ScoreController(Scorer scorer, ScoreView view)
        {
            _scorer = scorer;
            _view = view;
        }

        public void Bind()
        {
            _scorer.OnScoreChanged += UpdateScore;
        }

        public void Expose()
        {
            _scorer.OnScoreChanged -= UpdateScore;
        }

        private void UpdateScore()
        {
            _view.UpdateValue(_scorer.Score);
        }
    }
}