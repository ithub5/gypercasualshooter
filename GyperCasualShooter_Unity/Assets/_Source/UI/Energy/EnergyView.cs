using UnityEngine;
using UnityEngine.UI;

namespace UI.Energy
{
    public class EnergyView : MonoBehaviour
    {
        [SerializeField] private Image energyBar;

        public void UpdateValue(float value) =>
            energyBar.fillAmount = value;
    }
}