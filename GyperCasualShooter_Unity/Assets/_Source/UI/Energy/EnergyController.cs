using PlayerSystem.Core;
using PlayerSystem.Data.Core;

namespace UI.Energy
{
    public class EnergyController
    {
        private PlayerEnergy _energy;

        private EnergyView _view;
        private PlayerEnergySO _model;

        public EnergyController(PlayerEnergy energy, EnergyView view, PlayerEnergySO model)
        {
            _energy = energy;
            _view = view;
            _model = model;
        }

        public void Bind()
        {
            _energy.OnEnergyChanged += UpdateEnergy;
        }

        public void Expose()
        {
            _energy.OnEnergyChanged -= UpdateEnergy;
        }

        private void UpdateEnergy()
        {
            _view.UpdateValue(_energy.CurrentAmount / _model.MaxAmount);
        }
    }
}